#!/usr/bin/env python3
import bs4 as bs
import requests as rq
import matplotlib as plt
import argparse
import numpy as np
import statistics
import math
parser = argparse.ArgumentParser()
parser.add_argument('--grade')
parser.add_argument('university',nargs='*')
args = parser.parse_args()
if not args.university or not len(args.university):
    unis_url = 'http://admlist.ru/'
    resp = rq.get(unis_url)
    soup = bs.BeautifulSoup(resp.content.decode('utf-8'),features='lxml')
    args.university = [a['href'].split('/')[0] for a in soup.find(class_='tableFixHead').findAll('a')]
for uni in args.university:
    if 'msu' in uni:
        print('Sorry msu hides its information')
        pass
    faculties_url = 'http://admlist.ru/{}/'.format(uni)
    resp = rq.get(faculties_url)
    soup = bs.BeautifulSoup(resp.content.decode('utf-8'),features='lxml')
    print(soup.html.body.find('h1',recursive=False).center.text)
    faclinks =soup.find('tbody').findAll('a')
    score_sum = []
    stat_data_list={}
    var_data_total=False
    def var_data(name,total_fn=statistics.mean):
        stat_data_list[name]=[]
        return lambda l:(((stat_data_list[name].append(stat_data[name]),stat_data[name])[-1]) if not var_data_total else total_fn(stat_data_list[name]))
    stat={
        'N':len,
        'avg':statistics.mean,
        'min':min,
        'max':max,
        'mode':statistics.mode,
        'median':statistics.median,
        'admission':var_data('admission',sum),
        'passing_grade':var_data('passing_grade',min)
    }
    stat_data={
        'admission':0,
        'passing_grade':math.inf
        
    }
    def print_stat(l,tabs=0):
        for s in stat:
            print('\t'*tabs+s+":",stat[s](l))
    for link in faclinks:
        fac_sum=[]
        data_soup = bs.BeautifulSoup(rq.get(faculties_url+link['href']).content.decode('utf-8'),features='lxml')
        heading_row=data_soup.find_all(class_='tableFixHead')[0].thead.tr.findAll('th')
        score_idx = [i for i,x in enumerate(heading_row) if x.text == '∑' ][0]
        data_rows = data_soup.find_all(class_='tableFixHead')[0].tbody.findAll('tr')
        passing_grade=0
        kcp=int(data_soup.html.body.table.find_all('tr')[1].contents[0].text)
        stat_data['admission']=kcp
        for row in data_rows:
            try:
                score = row.contents[score_idx].text
                #print("\t"+score)
                if int(score)>10:
                    score_sum.append(int(score))
                    fac_sum.append(int(score))
                    if int(row.contents[2].text) == kcp:
                        passing_grade=int(score)
            except IndexError:
                pass
        stat_data['passing_grade']=passing_grade if passing_grade else math.inf
        if len(fac_sum) and (args.grade is None or int(args.grade) >=stat_data['passing_grade']):
            print('\t'+link.text)
            print_stat(fac_sum,2)
    print("\t==Overall statistics==")
    var_data_total=True
    print_stat(score_sum,2)
